# Instructions for setting up a Local Development Environment (LDE) for VTechWorks
July 2015

## Purpose
To quote Rachel Andrew's article ["A Simple Workflow from Development to Deployment"](http://www.smashingmagazine.com/2015/07/09/development-to-deployment-workflow/), "When designing and developing your website, you should try to match the live web server as much as possible. This should include ensuring that paths from root don’t change between local and live versions, and that [modules] and permissions are the same in both places. This approach will reduce the possibility of something going wrong as you push the site live. It should also enable you to come back to a site to make changes and updates and know that you can then deploy those changes without breaking the running site. A good local development environment saves you time and stress. It gives you a place to test things out."

## Step 1: Install prerequisite tools
Note that you will probably need at least 8Gb of memory on your computer to install and run the following.

* Download and install [VirtualBox](https://www.virtualbox.org/wiki/Downloads) for your operating system
(Windows, OSX, or Linux). VirtualBox will allow your desktop or laptop computer to function like a
[web server](https://en.wikipedia.org/?title=Web_server) rather than as
a [client](https://en.wikipedia.org/wiki/Client_%28computing%29).  

*  Download and install [Vagrant](http://vagrantup.com) for your operating system. Vagrant is a tool for managing virtual servers such as the one you will create with VirtualBox.

*  Check permissions on the .vagrant.d directory in your home directory:
```
$ ls -al
```

* You should be the owner of the directory. If not, run the following command (assuming that "jdoe" is your username on your computer):
```
$ sudo chown -R jdoe .vagrant.d/
```

* If you are using OS X, install [Homebrew](http://brew.sh). Homebrew is a [package manager](https://en.wikipedia.org/wiki/Package_manager) for OS X that comes with many tools that developers need. To install Homebrew, paste the following at the command line:
```
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* Install [Ansible](http://ansible.com). Ansible is a tool that allows automatic configuration and deployment of complex systems like dSpace. If you are using OS X, paste the following at the command line:
```
$ brew install ansible
```

* Install [Java](https://java.com/en/download/) and the [Java Development Kit](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) (and make sure you do so before you install Maven). Java is the language dSpace is written in, as well as a runtime environment, virtual machine, and development kit for your computer. The following command will also install the Java Development Kit (Oracle JDK). If you are using OS X, paste the following at the command line:
```
$ brew install Caskroom/cask/java
```

* Install [Maven](https://maven.apache.org/). Maven is a software management tool that "can manage a project's build, reporting and documentation from a central piece of information." The following command will install version 3.2 of Maven. If you are using OS X, paste the following at the command line:
```
$ brew install homebrew/versions/maven32
```
If you find you need another version of Maven, you can use the following commands to install the correct version:
```
$ brew unlink maven
$ brew install homebrew/versions/[maven version here]
```

* Install [git](https://git-scm.com/). Git is a source control management tool, aka a versioning tool. It allows multiple people to make changes to the code while keeping track of every change and allowing easy reversion. See [https://git-scm.com/book/en/v2/Getting-Started-Installing-Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) for instructions.

## Step 2: Get a copy of the codebase
Now that you have all the tools necessary to do development on VTechWorks's DSpace code, you need a copy of the code itself.

* Make sure you can log in to the VT GitLab service at [https://git.it.vt.edu](https://git.it.vt.edu). Edit your profile: add an avatar and contact info.

* Set up a public SSH key on your machine. The public SSH key is a bit like a password: it allows you to authenticate yourself so that you can make changes to the GitLab codebase from your computer. Follow the instructions at [https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key](https://git-scm.com/book/en/v2/Git-on-the-Server-Generating-Your-SSH-Public-Key) You don't have to create a passphrase when prompted (just hit enter), but if you do, make sure to remember or record it!

* Open the file id_rsa.pub in a text editor and copy the whole text to your clipboard.

* Add your public SSH key to the GitLab repo at [https://git.it.vt.edu/it-services/vtlibans_dspace](https://git.it.vt.edu/it-services/vtlibans_dspace). Click on your profile avatar in the top right and then click "Edit Profile Settings", or else go directly to your Profile Settings at [https://git.it.vt.edu/profile](https://git.it.vt.edu/profile). Click the 'SSH Keys' tab. Give your SSH Key a title (like "My VT Computer") and then paste in the full text of the file id_rsa.pub.

* Clone the VTechWorks git repository by pasting the following command:
```
$ git clone git@git.it.vt.edu:it-services/vtlibans_dspace.git
```
This repo is accessible only from Virginia Tech IP addresses.

* Once you have cloned the vtlibans_dspace repository, go to the directory:
```
$ cd vtlibans_dspace
```
* Within the vtlibans_dspace directory, clone [VTUL's fork of DSpace](https://github.com/vtul/vtechworks.git):
```
$ git clone git@github.com:VTUL/vtechworks.git dspace
```
Note that the [Vagrantfile](Vagrantfile) expects [VTUL's fork of DSpace](https://github.com/vtul/vtechworks.git)
to be on your host machine at `dspace` of your cloned repo. Don't use any other directory name.

* Then, from the `vtechworks` directory, clone the Dspace source using Maven.
```
$ cp templates/dspacedevvm.local.vt.edu.properties dspace
$ cd dspace
$ git checkout dspace-5_x
$ mvn package -Denv=dspacedevvm.local.vt.edu -Dmirage2.on=true
```

* Then, from the `vtechworks` directory:
```
$ cp vars.TEMPLATE.yml vars.yml
$ vagrant up
```
* Go get a cup of coffee! This will take awhile.

## Step 3: Set up your Virtual Machine

* Login to your VM to import localizations.
```
$ vagrant ssh
```

* We will be deleting the default database to install a sample of records. So, first, stop the Tomcat server.
```
$ sudo /usr/sbin/service tomcat7 stop
```

* The following steps will require becoming superuser. Make yourself a superuser.
```
$ sudo -s
```

* Switch to the dspace bin directory.
```
$ cd /dspace/bin
```

* Empty the demo dspace database. When asked if you want to PERMANENTLY DELETE everything from the database, type 'y' for 'yes.'
```
$ ./dspace database clean
```

* Import the sample records (you will need to request this; there is a zipped sample file in the VTechWorks Team Google Drive folder).
```
$ sudo -u postgres psql dspace < /vagrant/20150616_dspace5_dump.sql
```

* Make sure it is the right database.
```
$ ./dspace database info
```

* Update it if needed.
```
$ ./dspace database migrate
```

* Restart the tomcat7 server
```
$ sudo /usr/sbin/service tomcat7 restart
```

* Congratulations! You can view your installation at http://192.168.60.4

* When through with session, exit as root, exit as user
```
$ exit
$ exit
```

* Stop Vagrant
```
$ stop vagrant
```

* Close the virtual machine OR write the contents of the virtual machine's RAM to the host's hard drive.
```
$ vagrant halt #closes virtual machine or
$ vagrant suspend # write contents of virtual machine RAM to host hard drive
```

## Further Resources

* The How-To Geek. ["Command Line Basics"](http://lifehacker.com/5633909/who-needs-a-mouse-learn-to-use-the-command-line-for-almost-anything). _LifeHacker_ 9/9/10.
* William E. Shotts Jr. [_The Linux Command Line_](http://proquest.safaribooksonline.com/9781593273897). No Starch Press 2012. Available for free online through the VT Library's Safari Books Online subscription.
* ["Try Git"](https://www.codeschool.com/courses/try-git). Code School. Free interactive git tutorial.
* Richard E. Silverman. [_Git Pocket Guide_](http://proquest.safaribooksonline.com/9781449327507). O'Reilly Media 2013. Available for free online through the VT Library's Safari Books Online subscription.
* Mitchell Hashimoto. [_Vagrant: Up and Running_](http://proquest.safaribooksonline.com/9781449336103). O'Reilly Media 2013. Available for free online through the VT Library's Safari Books Online subscription.
* [DSpace 5.x Documentation](https://wiki.duraspace.org/display/DSDOC5x/DSpace+5.x+Documentation).
