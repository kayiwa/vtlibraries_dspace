---
##
# Install prerequisites
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: ensure apt cache repository is up to date
      apt: update_cache=yes
    - name: ensure dspace dependencies are installed
      apt: pkg={{item}}
      with_items:
        - ant
        - maven
        - apache2-mpm-prefork
        - apache2
        - postgresql
        - postgresql-contrib
        - tomcat7
        - libtcnative-1
        - opensmtpd
        - python-pycurl
        - python-software-properties
        - python-psycopg2
        - git
        - libapache2-mod-shib2
        - libshibsp-dev

    - name: add JAVA_HOME environment variables
      lineinfile: dest=/etc/environment line="JAVA_HOME=/usr/lib/jvm/java-7-oracle"

    - name: download apache ant binary
      command: wget http://apache.mirrors.hoobly.com//ant/binaries/{{ant_version}} chdir=/tmp creates=/tmp/{{ant_version}}

    - name: unzip ant binary
      command: tar xf /tmp/{{ant_version}} -C /opt creates=/opt/apache-ant

    - name: override system ant mvn modules
      shell: echo 'export PATH="/opt/apache-ant/bin:$PATH"' > /etc/profile.d/ant.sh creates=/etc/profile.d/ant.sh

    - name: download maven 3.2.5
      get_url: url=http://apache.arvixe.com/maven/maven-3/3.2.5/binaries/apache-maven-3.2.5-bin.tar.gz dest=/tmp/apache-maven-3.2.5-bin.tar.gz

    - name: untar downloaded maven file
      command: tar xf /tmp/apache-maven-3.2.5-bin.tar.gz -C /opt creates=/opt/apache-maven-3.2.5

    - name: create a symbolic link
      file:
        src: "/opt/apache-maven-3.2.5/bin/mvn"
        dest: "/usr/bin/mvn"
        state: link

# clone afrench repository
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: create vtechworks group
      group: name=vtechworks state=present gid=997

    - name: create vtechworks user
      user: name=vtechworks group=vtechworks shell=/bin/bash home=/home/vtechworks uid=998

    - name: add vtech user to tomcat group
      user: name=vtechworks groups=tomcat7

    - name: create .m2 folder
      file: path=/home/vtechworks/.m2/ state=directory owner=vtechworks

    - name: tomcat | prepare maven settings dir
      file: dest=/usr/share/tomcat7/.m2 owner=tomcat7 group=tomcat7 mode=770 state=directory



# Configure PostgreSQL
- hosts: all
  sudo: yes
  sudo_user: postgres

  vars_files:
    - vars.yml

  tasks:
    - name: postgres | add dspace user
      postgresql_user: name={{dbuser}} password={{dbpass}}

    - name: postgres | add dspace database
      postgresql_db: name={{dbname}} encoding='UTF-8' template='template0' owner={{dbuser}}

    - name: postgres | configure md5 auth for dspace database
      template: src=templates/pg_hba.conf.j2 dest=/etc/postgresql/9.3/main/pg_hba.conf

    - name: postgres | restart
      service: name=postgresql state=restarted
      tags: postgres

##
# Install Oracle JDK7
# see: http://www.webupd8.org/2012/01/install-oracle-java-jdk-7-in-ubuntu-via.html
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: jdk7 | add webupd8team ppa
      apt_repository: repo='ppa:webupd8team/java' update_cache=yes

    - name: jdk7 | accept Oracle license
      shell: echo oracle-java7-installer shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections

    - name: jdk7 | install
      action: apt pkg={{item}}
      with_items:
        - oracle-java7-installer
        - oracle-java7-set-default

# build maven

- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: clone the repository
      git: repo=https://github.com/amandafrench/VT-DSpace.git dest={{ dspace_source_dir }} version=dspace-5_x

    - name: change permissions on the cloned directory
      file: dest={{ dspace_source_dir }} state=directory owner=vtechworks group=tomcat7

    - name: copy properties file needed for maven
      template: src=templates/dspacedevvm.local.vt.edu.properties dest={{ dspace_source_dir }}

    - name: run maven shell script
      command: mvn package -Denv=dspacedevvm.local.vt.edu -Dmirage2.on=true chdir={{dspace_source_dir}}
      become_user: tomcat7



##
# Configure Tomcat
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: tomcat | stop
      service: name=tomcat7 state=stopped

    - name: tomcat | configure server.xml
      template: src=templates/server.xml.j2 dest=/etc/tomcat7/server.xml

    - name: tomcat | configure tomcat defaults
      template: src=templates/tomcat7.j2 dest=/etc/default/tomcat7

    - name: tomcat | change tomcat shell to bash
      user: name=tomcat7 shell=/bin/bash

    - name: tomcat | prepare maven settings dir
      file: dest=/usr/share/tomcat7/.m2 owner=tomcat7 group=tomcat7 mode=770 state=directory

    - name: tomcat | prepare dspace source dir
      file: dest={{dspace_source_dir}} owner=tomcat7 group=tomcat7 mode=770 state=directory

    - name: tomcat | prepare webapps dir
      file: dest={{dspace_install_dir}} owner=tomcat7 group=tomcat7 state=directory


##
# Configure Apache
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: httpd | disable default site
      command: a2dissite 000-default
      tags: httpd
    - name: httpd | enable modules
      command: a2enmod rewrite headers proxy proxy_http expires
      tags: httpd
    - name: httpd | copy vhost config
      template: src=templates/dspace.j2 dest=/etc/apache2/sites-available/dspacedevvm.conf
      tags: httpd
    - name: httpd | enable dspace site
      command: a2ensite dspacedevvm
      tags: httpd
    - name: httpd | restart
      service: name=apache2 state=restarted
      tags: httpd
    - name: apache enable shibboleth module
      apache2_module: name=shib2 state=present
      notify: restart apache

##
# DSpace
- hosts: all
  sudo: yes
  sudo_user: tomcat7

  vars_files:
    - vars.yml

  tasks:
    - name: dspace | perform fresh install
      command: /opt/apache-ant/bin/ant fresh_install chdir={{dspace_source_dir}}/dspace/target/dspace-installer creates={{dspace_install_dir}}/webapps/xmlui
      tags: dspace
    - name: dspace | set up tomcat ROOT context
      shell: echo '<Context docBase="{{dspace_install_dir}}/webapps/xmlui"/>' > /var/lib/tomcat7/conf/Catalina/localhost/ROOT.xml
      tags: dspace
    - name: dspace | set up other tomcat contexts
      shell: echo '<Context docBase="{{dspace_install_dir}}/webapps/{{item}}"/>' > /var/lib/tomcat7/conf/Catalina/localhost/{{item}}.xml
      with_items:
        - solr
        - oai
        - swordv2
      tags: dspace

##
# Restart Tomcat after webapp installation
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:

    - name: tomcat | start
      service: name=tomcat7 state=started
      tags: tomcat

##
# Misc utils
- hosts: all
  sudo: yes

  vars_files:
    - vars.yml

  tasks:
    - name: Install misc utilities
      apt: pkg={{item}}
      with_items:
        - xpdf
        - lrzip
        - iotop
        - mtr
        - links
        - htop
        - vim
        - libmagick++-dev
        - imagemagick
        - ghostscript
        - nfs-common
      tags: misc

    - name: tomcat | start
      service: name=tomcat7 state=restarted

# vim: set sw=2 ts=2:
