#!/bin/sh
dspace_source=/usr/share/tomcat7/src/git/DSpace/dspace/target/dspace-installer
ant_build=/usr/share/tomcat7/src/git/DSpace/dspace/target/dspace-installer/dspace/target/dspace-installer
# switch to source directory
cd $dspace_source

# ensure correct permissions
chown -R vtechworks:tomcat7 $dspace_source

# Pull from repo
/usr/bin/sudo -H -su vtechworks /usr/bin/git pull

# rebuild with maven
sudo -H -su vtechworks /usr/bin/mvn -U clean package -Denv=dspacedevvm.local.vt.edu -Dmirage2.on=true

# permissions to tomcat
chown -R tomcat7:vtechworks $dspace_source

# ant update as user tomcat
cd $ant_build

/usr/sbin/service tomcat7 stop

# permissions to dspace home
chown -R tomcat7:tomcat7 /dspace/{bin,config,etc,exports,handle-server,lib,log,reports,solr,search,temp,webapps,triplestore,var}

# update ant
sudo -H -su tomcat7 /usr/bin/ant update

# restart tomcat
/usr/sbin/service tomcat7 start
