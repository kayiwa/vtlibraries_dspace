#!/bin/bash
# export JAVA_HOME
export JAVA_HOME=/usr/lib/jvm/java-7-oracle

# cd into dspace source
cd /usr/share/tomcat7/src/git/DSpace/dspace/target/dspace-installer

# build the maven
mvn package -Denv=dspacedevvm.local.vt.edu -Dmirage2.on=true
